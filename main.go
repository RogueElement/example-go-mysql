package main

import (
	"context"
	"log"
	"os"
	"os/signal"

	"github.com/siddontang/go-mysql/client"
	"github.com/siddontang/go-mysql/mysql"
	"github.com/siddontang/go-mysql/replication"
)

func main() {
	username, password, db := "root", "pass", "db"

	conn, err := client.Connect("127.0.0.1:3306", username, password, db)

	if err != nil {
		log.Println(err.Error())
		return
	}

	conn.Execute("FLUSH TABLES WITH READ LOCK;")

	err = conn.Ping()
	if err != nil {
		println(err)
	}

	r, _ := conn.Execute(`SHOW MASTER STATUS;`)

	// Extract the log file name and log file position from the query result
	// --skipping error checks because this is an explanation script
	binlogFile, _ := r.GetString(0, 0)
	binlogPos, _ := r.GetUint(0, 1)

	conn.Close()

	cfg := replication.BinlogSyncerConfig{
		ServerID: 100,
		Flavor:   "mysql",
		Host:     "127.0.0.1",
		Port:     3306,
		User:     username,
		Password: password,
	}

	syncer := replication.NewBinlogSyncer(cfg)

	streamer, err := syncer.StartSync(mysql.Position{binlogFile, uint32(binlogPos)})

	if err != nil {
		return
	}

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		<-c
		syncer.Close()
		os.Exit(0)
	}()

	for {
		ev, err := streamer.GetEvent(context.Background())
		if err != nil {
			continue
		}
		ev.Dump(os.Stdout)
	}
}
